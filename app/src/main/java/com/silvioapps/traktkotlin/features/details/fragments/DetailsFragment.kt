package com.silvioapps.traktkotlin.features.details.fragments

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvioapps.traktkotlin.R
import com.silvioapps.traktkotlin.databinding.FragmentDetailsBinding
import com.silvioapps.traktkotlin.features.list.models.ListModel
import com.silvioapps.traktkotlin.features.shared.fragments.CustomFragment

class DetailsFragment : CustomFragment(){
    private var fragmentDetailsBinding : FragmentDetailsBinding? = null

    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View? {
        fragmentDetailsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_details, viewGroup, false)
        fragmentDetailsBinding?.listModel = arguments?.getSerializable("details") as ListModel

        showBackButton(fragmentDetailsBinding?.toolBar!!, fragmentDetailsBinding?.listModel?.movie?.title!!)

        return fragmentDetailsBinding?.root
    }
}
