package com.silvioapps.traktkotlin.features.list.models

import java.io.Serializable

class Movie : Serializable {
	val year: Int? = null
	val ids: Ids? = null
	val title: String? = null
	var poster: String? = null
	var plot: String? = null
	var director: String? = null
}