package com.silvioapps.traktkotlin.features.details.activities

import android.os.Bundle
import com.silvioapps.traktkotlin.R
import com.silvioapps.traktkotlin.features.details.fragments.DetailsFragment
import com.silvioapps.traktkotlin.features.shared.views.activities.CustomActivity

class DetailsActivity : CustomActivity() {

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if(savedInstanceState == null) {
            val detailsFragment = DetailsFragment()
            detailsFragment.setArguments(intent.getBundleExtra("data"))

            attachFragment(R.id.frameLayout, detailsFragment)
        }
    }

    override fun onSupportNavigateUp() : Boolean{
        onBackPressed()
        return true
    }
}
