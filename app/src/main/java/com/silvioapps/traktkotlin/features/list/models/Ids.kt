package com.silvioapps.traktkotlin.features.list.models

import java.io.Serializable

class Ids : Serializable {
	val tmdb: Int? = null
	val imdb: String? = null
	val trakt: Int? = null
	val slug: String? = null
}