package com.silvioapps.traktkotlin.features.list.services

import com.silvioapps.traktkotlin.features.list.models.ListModel
import com.silvioapps.traktkotlin.constants.Constants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ListService {
    @Headers("Content-type: ${Constants.CONTENT_TYPE}",
        "trakt-api-key: ${Constants.TRAKT_API_KEY}",
        "trakt-api-version: ${Constants.TRAKT_API_VERSION}")
    @GET(Constants.LIST)
    fun getList(@Query("query") search : String, @Query("years") years : Int, @Query("page") page : Int) : Call<MutableList<ListModel>>
}