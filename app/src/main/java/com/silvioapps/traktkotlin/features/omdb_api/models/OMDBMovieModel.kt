package com.silvioapps.traktkotlin.features.omdb_api.models

class OMDBMovieModel(
    val Metascore: String? = null,
    val BoxOffice: String? = null,
    val Website: String? = null,
    val ImdbRating: String? = null,
    val ImdbVotes: String? = null,
    val Runtime: String? = null,
    val Language: String? = null,
    val Rated: String? = null,
    val Production: String? = null,
    val Released: String? = null,
    val ImdbID: String? = null,
    val Plot: String? = null,
    val Director: String? = null,
    val Title: String? = null,
    val Actors: String? = null,
    val Response: String? = null,
    val Type: String? = null,
    val Awards: String? = null,
    val DVD: String? = null,
    val Year: String? = null,
    val Poster: String? = null,
    val Country: String? = null,
    val Genre: String? = null,
    val Writer: String? = null
)
