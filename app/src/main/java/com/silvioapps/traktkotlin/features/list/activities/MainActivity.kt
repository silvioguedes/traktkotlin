package com.silvioapps.traktkotlin.features.list.activities

import com.silvioapps.traktkotlin.features.shared.views.activities.CustomActivity
import android.os.Bundle
import com.silvioapps.traktkotlin.R
import com.silvioapps.traktkotlin.features.list.fragments.MainFragment

class MainActivity : CustomActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null) {
            attachFragment(R.id.frameLayout, MainFragment())
        }
    }
}
