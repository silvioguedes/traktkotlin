package com.silvioapps.traktkotlin.features.list.adapters

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.silvioapps.traktkotlin.BR
import com.silvioapps.traktkotlin.R
import com.silvioapps.traktkotlin.constants.Constants
import com.silvioapps.traktkotlin.features.list.models.ListModel
import com.silvioapps.traktkotlin.features.omdb_api.models.OMDBMovieModel
import com.silvioapps.traktkotlin.features.omdb_api.services.OMDBService
import com.silvioapps.traktkotlin.features.shared.listeners.ViewClickListener
import com.silvioapps.traktkotlin.features.shared.services.ServiceGenerator
import com.silvioapps.traktkotlin.features.shared.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import kotlin.collections.List

class ListAdapter(list_ : List<ListModel>, viewClickListener_ : ViewClickListener) : RecyclerView.Adapter<ListAdapter.BindingViewHolder>() {
    private var list = listOf<ListModel>()

    companion object{
        private var viewClickListener : ViewClickListener? = null
    }

    init{
        this.list = list_
        viewClickListener = viewClickListener_
    }

    class BindingViewHolder(view : View) : RecyclerView.ViewHolder(view){
        var viewDataBinding : ViewDataBinding? = null

        init{
            viewDataBinding = DataBindingUtil.bind<ViewDataBinding>(view)
            Utils.setClickListeners(view, viewClickListener)
        }
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType: Int) : BindingViewHolder{
        val view = LayoutInflater.from(parent.context).inflate(R.layout.main_list_layout, parent, false)
        return BindingViewHolder(view)
    }

    override fun onBindViewHolder(holder : BindingViewHolder, position : Int) {
        Utils.setTags(position, holder.itemView)

        if(list.size > position) {
            val listModel : ListModel = list.get(position)

            val service : OMDBService = ServiceGenerator.createService(Constants.OMDB_API_BASE_URL, Constants.TIMEOUT, OMDBService::class.java)
            val call : Call<OMDBMovieModel> = service.getOMDBMovie(Constants.OMDB_API_KEY, listModel.movie?.ids?.imdb ?: "")
            call.enqueue(object : Callback<OMDBMovieModel> {
                override fun onResponse(call : Call<OMDBMovieModel>, response : Response<OMDBMovieModel>) {
                    listModel.movie?.poster = response.body()?.Poster!!
                    listModel.movie?.plot = response.body()?.Plot!!
                    listModel.movie?.director = response.body()?.Director!!

                    holder.viewDataBinding?.setVariable(BR.listModel, listModel)
                    holder.viewDataBinding?.executePendingBindings()
                }

                override fun onFailure(call : Call<OMDBMovieModel>, t : Throwable) {}
            })

            holder.viewDataBinding?.setVariable(BR.listModel, listModel)
        }
    }

    override fun getItemCount() : Int{
        return list.size
    }
}
