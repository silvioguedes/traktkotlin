package com.silvioapps.traktkotlin.features.omdb_api.services

import com.silvioapps.traktkotlin.constants.Constants
import com.silvioapps.traktkotlin.features.omdb_api.models.OMDBMovieModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OMDBService {
    @GET(Constants.OMDB_MOVIE)
    fun getOMDBMovie(@Query("apikey") apikey : String, @Query("i") i : String) : Call<OMDBMovieModel>
}