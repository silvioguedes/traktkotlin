package com.silvioapps.traktkotlin.features.list.models

import java.io.Serializable

class ListModel : Serializable{
	val score: Double? = null
	val movie: Movie? = null
	val type: String? = null
	var showLoading : Boolean = false
}