package com.silvioapps.traktkotlin.constants

class Constants {
    companion object {
        const val TRAKT_API_BASE_URL = "https://api.trakt.tv"
        const val LIST = "/search/movie/"
        const val TIMEOUT : Long = 15
        const val CONTENT_TYPE = "application/json"
        const val TRAKT_API_KEY = "84f807cee109401c71c563111c830397f48a48f632a7edf1126825631d6dc78b"
        const val TRAKT_API_VERSION = 2
        const val OMDB_API_BASE_URL = "http://www.omdbapi.com"
        const val OMDB_MOVIE = "/"
        const val OMDB_API_KEY = "9a2a3e65"
    }
}
